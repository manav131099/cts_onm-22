rm(list = ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN070CHistorical.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/IN070CDigest/summaryFunctions.R')
RESETHISTORICAL=1
daysAlive = 0
reorderStnPaths = c(6,4,5,7,8,9,10,11)

if(RESETHISTORICAL)
{
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-070C]')
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-070C]')
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-070C]')
  
}

path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-070C]"
pathwrite2G = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-070C]"
pathwrite3G = "/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-070C]"
pathwrite4G = "/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-070C]"


checkdir(pathwrite2G)
checkdir(pathwrite3G)
checkdir(pathwrite4G)
years = dir(path)
for(x in 4 : length(years))
{
  pathyr = paste(path,years[x],sep="/")
  pathwriteyr = paste(pathwrite2G,years[x],sep="/")
  checkdir(pathwriteyr)
  months = dir(pathyr)
  if(!length(months))
    next
  for(y in 8 : length(months))
  {
    pathmon = paste(pathyr,months[y],sep="/")
    print(pathmon)
    pathwritemon = paste(pathwriteyr,months[y],sep="/")
    checkdir(pathwritemon)
    stns = dir(pathmon)
    print(stns)
    if(!length(stns))
      next
    stns = stns[reorderStnPaths]
    print(stns)
    for(z in 1 : length(stns))
    {
      type = 1
      pathstn = paste(pathmon,stns[z],sep="/")
      if(grepl("ESS",stns[z]))
        type = 0
      if(stns[z] == "WMS")
        type = 2
      pathwritestn = paste(pathwritemon,stns[z],sep="/")
      checkdir(pathwritestn)
      days = dir(pathstn)
      if(!length(days))
        next
      for(t in 1 : length(days))
      {
        if(z == 1)
          daysAlive = daysAlive + 1
        pathread = paste(pathstn,days[t],sep="/")
        pathwritefile = paste(pathwritestn,days[t],sep="/")
        if(RESETHISTORICAL || !file.exists(pathwritefile))
        {
          secondGenData(pathread,pathwritefile,type)
          print(paste(days[t],"Done"))
        }
      }
    }
  }
}
sink()
