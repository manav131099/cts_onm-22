errHandle = file('/home/admin/Logs/LogsIN013Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/IN013Digest/HistoricalAnalysis2G3GIN013.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')

initDigest = function(df,no)
{
  body = "\n\n******************************************\n\n"
  body = paste(body," ",as.character(df[,1])," Meter-",no,sep="")
  body = paste(body,"\n\n******************************************\n\n")
	body = paste(body,"Eac",no,"-1 [kWh]: ",as.character(df[,2]),sep="")
  body = paste(body,"\n\nEac",no,"-2 [kWh]: ",as.character(df[,3]),sep="")
  body = paste(body,"\n\nDaily specific yield [kWh/kWp]:",as.character(df[,6]))
  
	{
	if(no==1)
		body = paste(body,"\n\nPR [%]:",as.character(df[,10]))
	else if(no==2)
		body = paste(body,"\n\nPR [%]:",as.character(df[,9]))
	}

	body = paste(body,"\n\nRatio EAC","-2/EAC","-1: ",round((as.numeric(df[,3])/as.numeric(df[,2]))*100,2),sep="")
  acpts = round(as.numeric(df[,4]) * 14.4)
  body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,4]),"%)",sep="")
  body = paste(body,"\n\nDowntime (%):",as.character(df[,5]))
  body = paste(body,"\n\nLast recorded timestamp:",as.character(df[,7]))
  body = paste(body,"\n\nLast recorded energy meter reading [kWh]:",as.character(df[,8]))
	if(no==1)
	{
  body = paste(body,"\n\nTotal daily irradiation [kWh/m2]:",as.character(df[,9]))
  body = paste(body,"\n\nMean module temperature [C]:",as.character(df[,11]))
	}
	#PRPRINT = round((as.numeric(df[,6])*100/as.numeric(df[,9])),1)
  body = paste(body,"\n******************************************\n")
  return(body)
}
printtsfaults = function(TS,body)
{
	if(length(TS) > 1)
	{
		body = paste(body,"\n**************************************************\n")
		body = paste(body,paste("\nTimestamps","where Pac < 1 between 8am - 6pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
		body = paste(body,"\n**************************************************\n")
	}
	return(body)
}
sendMail = function(df1,df2,pth1,pth2,pth3)
{
  filetosendpath = pth3
  if(file.exists(pth3))
	{
  filetosendpath = c(pth1,pth2,pth3)
  }
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	print('Filenames Processed')
	body = "Site Name: CTS MLCP Chennai
	\nLocation: Chennai, India
	\nO&M Code: IN-013
	\nSystem Size: 472.5 kWp
	\nNumber of Energy Meters: 2
	\nCapacity Meter-1: 252 kWp
	\nCapacity Meter-2: 220.5 kWp
	\nModule Brand / Model / Nos: Canadian Solar/315W/1500
	\nInverter Brand / Model / Nos: SMA 25kW/15"
	body = paste(body,"\n\nSystem age [days]:",as.character((0+as.numeric(DAYSALIVE))))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((0+as.numeric(DAYSALIVE))/365,2)))
  body = paste(body,initDigest(df1,1),sep="\n\n")
  body = printtsfaults(TIMESTAMPSALARM1,body)
	body = paste(body,initDigest(df2,2))
  body = printtsfaults(TIMESTAMPSALARM2,body)
	print('2G data processed')
  body = paste(body,"\n\n\n******************************************\n\n")
  body = paste(body,"Station History")
  body = paste(body,"\n\n******************************************\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive:",yrsalive)
#  body = paste(body,"\n\n# Eac1 total this month [kWh]:",MONTHTOTEAC1)
#  body = paste(body,"\n\n# Eac2 total this month [kWh]:",MONTHTOTEAC2)
#  proje1 = format(round((MONTHTOTEAC1 * DAYSTHISMONTHAC / DAYSTHISMONTH),1),nsmall=1)
#  proje2 = format(round((MONTHTOTEAC2 * DAYSTHISMONTHAC / DAYSTHISMONTH),1),nsmall=1)
#  body = paste(body,"\n\n# Eac1 projected total for the month [kWh]:",proje1)
#  body = paste(body,"\n\n# Eac2 projected total for the month [kWh]:",proje2)
	if(file.exists(pth3))
	{
#	df = read.table(pth3,header =T,sep="\t")
#	avge1 = format(round(mean(as.numeric(df[,2])),1),nsmall=1)
#  avge2 = format(round(mean(as.numeric(df[,3])),1),nsmall=1)
#	body = paste(body,"\n# Eac1 average for the month [kWh]:",avge1)
#  body = paste(body,"\n# Eac2 average for the month [kWh]:",avge2)
  }
#	body = paste(body,"\n# EAC1 sum last 30 days [kWh]:",sum(LAST30DAYSEAC1))
#  body = paste(body,"\n# EAC2 sum last 30 days [kWh]:",sum(LAST30DAYSEAC2))
#  body = paste(body,"\n# EAC1 average last 30 days [kWh]:",round(mean(LAST30DAYSEAC1),1))
#  body = paste(body,"\n# EAC2 average last 30 days [kWh]:",round(mean(LAST30DAYSEAC2),1))
#  body = paste(body,"\n EAC1/EAC2 lifetime ratio :",round((EAC1GLOB/EAC2GLOB),4))
#  body = paste(body,"\n\n******************************************")
	print('3G data processed')
	Sys.sleep(20)
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-013X] Digest",substr(currday,14,23)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            file.names = filenams, # optional parameter
            debug = F)
	recordTimeMaster("IN-013X","Mail",substr(currday,14,23))
}
sender = c('operations@cleantechsolar.com')
uname = c('shravan.karthik@cleantechsolar.com')
recipients = getRecipients("IN-013X","m")
#recipients = c('shravan1994@gmail.com')

pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	recipients = getRecipients("IN-013X","m")
	recordTimeMaster("IN-013X","Bot")
  sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathstations = paste(pathyear,months[y],sep="/")
			stationsread = dir(pathstations)
			pathdays1 = paste(pathstations,stationsread[1],sep="/")
			pathdays2 = paste(pathstations,stationsread[2],sep="/")
      writepath2Gstations = paste(writepath2Gyr,months[y],sep="/")
      writepath2Gdays = paste(writepath2Gstations,stationsread[1],sep="/")
      writepath2Gdays2 = paste(writepath2Gstations,stationsread[2],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[IN-013X] ",months[y],".txt",sep="")
      checkdir(writepath2Gstations)
      checkdir(writepath2Gdays)
      checkdir(writepath2Gdays2)
      days = dir(pathdays1)
			days2 = dir(pathdays2)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
#        MONTHTOTEAC1 = 0
#        MONTHTOTEAC2 = 0
#        DAYSTHISMONTH = 0
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
#        DAYSTHISMONTHAC = monthDays(temp)
      }
      for(t in startdays : length(days))
      {
         condn11  = (t != length(days))
         condn12 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         condn21  = (t != length(days2))
         condn22 = ((t == length(days2)) && ((y != length(months)) || (x != length(years))))
         if(!(condn11 || condn21 || condn22 || condn12))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
           Sys.sleep(3600)
           next
         }
				 print(paste('Processing',days[t]))
				 if(grepl("Copy",days[t]) || grepl("Copy",days2[t]))
				 {
				 	print('Copy file found so removing it')
					{
					if(grepl("Copy",days[t]))
						command = paste('rm "',pathdays1,'/',days[t],'"',sep="")
					else
						command = paste('rm "',pathdays2,'/',days2[t],'"',sep="")
					}
					print(command)
					system(command)
					print('Command complete.. sleeping')
				 	Sys.sleep(3600)
					next
				 }
				 todisp = 1
         sendmail = 1
#         DAYSALIVE = DAYSALIVE + 1
				 print(paste('pathdays is',pathdays1))
				 print(paste('t is',t))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
         readpath = paste(pathdays1,days[t],sep="/")
         readpath2 = paste(pathdays2,days2[t],sep="/")
#				 lastdata = read.table(readpath,header = T,sep = "\t")
#				 lastdata = c(as.character(lastdata[nrow(lastdata),1]),as.character(as.numeric(lastdata[nrow(lastdata),39])/1000))
         df1 = secondGenData(readpath,writepath2Gfinal,1)
         df2 = secondGenData(readpath2,writepath2Gfinal2,2)
         tots = thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath3Gfinal)
				 if(!is.finite(tots[1])) 
				 {
					 tots[1]=0
				 }
				 if(!is.finite(tots[2]))
				 {
			 		 tots[2]=0
			 	 }
 #       LAST30DAYSEAC1[[idxvec]] = tots[1]
 #       LAST30DAYSEAC2[[idxvec]] = tots[2]

 #        LAST30DAYSEAC1[[idxvec]] = tots[1]
 #        LAST30DAYSEAC2[[idxvec]] = tots[2]
 #        MONTHTOTEAC1 = MONTHTOTEAC1 + tots[1]
 #        MONTHTOTEAC2 = MONTHTOTEAC2 + tots[2]
 #        DAYSTHISMONTH = DAYSTHISMONTH + 1
 #        EAC1GLOB = EAC1GLOB + tots[1]
 #        EAC2GLOB = EAC2GLOB + tots[2]
 #        idxvec = (idxvec + 1) %% 30
 #        if(idxvec == 0)
 #        {
 #          idxvec = 1
 #        }
  if(sendmail ==0)
  {
    next
  }
	print('Sending Mail')
  sendMail(df1,df2,writepath2Gfinal,writepath2Gfinal2,writepath3Gfinal)
	print('Mail Sent')

      }
    }
  }
	gc()
}
sink()
