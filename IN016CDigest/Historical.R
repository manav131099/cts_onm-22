rm(list = ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN016CHistorical.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/IN016CDigest/summaryFunctions.R')
RESETHISTORICAL=1
daysAlive = 0
reorderStnPaths = c(6,12,15,18,1,2,3,4,5,7,8,9,10,11,13,14,16,17)

if(RESETHISTORICAL)
{
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-016C]')
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-016C]')
  system('rm -R /home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-016C]')  
}

path = "/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-016C]"
pathwrite2G = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-016C]"
pathwrite3G = "/home/admin/Dropbox/FlexiMC_Data/Third_Gen/[IN-016C]"
pathwrite4G = "/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-016C]"


checkdir(pathwrite2G)
checkdir(pathwrite3G)
checkdir(pathwrite4G)
years = dir(path)
for(x in 3 : length(years))
{
  pathyr = paste(path,years[x],sep="/")
  pathwriteyr = paste(pathwrite2G,years[x],sep="/")
  checkdir(pathwriteyr)
  months = dir(pathyr)
  if(!length(months))
    next
  for(y in 6 : length(months))
  {
    pathmon = paste(pathyr,months[y],sep="/")
    pathwritemon = paste(pathwriteyr,months[y],sep="/")
    checkdir(pathwritemon)
    stns = dir(pathmon)
    if(!length(stns))
      next
    stns = stns[reorderStnPaths]
    a=c("DI_B_MFM","PR_B_MFM","TE_C_MFM","UP_CR_MFM")
    for(z in 1 : length(stns))
    {
      type = 1
      pathstn = paste(pathmon,stns[z],sep="/")
      if(stns[z] %in% a)
        type = 0
      if(stns[z] == "WMS")
        type = 2
      pathwritestn = paste(pathwritemon,stns[z],sep="/")
      checkdir(pathwritestn)
      days = dir(pathstn)
      if(!length(days))
        next
      for(t in 1 : length(days))
      {
        if(z == 1)
          daysAlive = daysAlive + 1
        pathread = paste(pathstn,days[t],sep="/")
        pathwritefile = paste(pathwritestn,days[t],sep="/")
        if(RESETHISTORICAL || !file.exists(pathwritefile))
        {
          secondGenData(pathread,pathwritefile,type)
          print(paste(days[t],"Done"))
        }
      }
    }
  }
}
sink()
