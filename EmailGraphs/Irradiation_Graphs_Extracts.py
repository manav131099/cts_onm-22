import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc

#Seris
#minute
""" path_2='/home/admin/Dropbox/Cleantechsolar/1min/[728]/'
final_df=pd.DataFrame(columns=['Tm','Troom_MV'])
for i in sorted(os.listdir(path_2)):
    if(i=='2020'):
        for j in sorted(os.listdir(path_2+i)):
            if(j=='2020-05'):
                for k in sorted(os.listdir(path_2+i+'/'+j)):
                    try:
                        print(path_2+i+'/'+j+'/'+k)
                        df=pd.read_csv(path_2+i+'/'+j+'/'+k,sep='\t')
                        final_df=final_df.append(df[['Tm','Troom_MV','Hroom_MV','Act_Pwr-Tot_Pond','Aprn_Pwr-Tot_Pond','I-A_Pond','I-B_Pond','I-C_Pond','Act_Pwr-Tot_R02','Aprn_Pwr-Tot_R02','I-A_R02','I-B_R02','I-C_R02','Act_Pwr-Tot_R03','Aprn_Pwr-Tot_R03','I-A_R03','I-B_R03','I-C_R03','Act_Pwr-Tot_R11','Aprn_Pwr-Tot_R11','I-A_R11','I-B_R11','I-C_R11']],sort=False)
                    except:
                        print('Failed')
final_df.columns=['Timestamp','MV room Temperature (degC)']
final_df.to_csv('KH-008 Data Extract.txt',sep='\t',index=False) """

#Locus
""" path_2='/home/admin/Dropbox/Gen 1 Data/[TH-007L]/'
final_df=pd.DataFrame(columns=['ts','POAI_avg'])
for i in sorted(os.listdir(path_2)):
    if(i=='2020'):
        print(i)
        for j in sorted(os.listdir(path_2+i)):
            if(j=='2020-06'):
                print(j)
                for k in sorted(os.listdir(path_2+i+'/'+j)):
                    if('Py' in k):
                        print(k)
                        for l in sorted(os.listdir(path_2+i+'/'+j+'/'+k)):
                            try:
                                df=pd.read_csv(path_2+i+'/'+j+'/'+k+'/'+l,sep='\t')
                                final_df=final_df.append(df[['ts','POAI_avg']],sort=False)
                            except:
                                print('Failed')
final_df.to_csv('TH-007-MinuteAll-2019-06.txt',sep='\t',index=False) """