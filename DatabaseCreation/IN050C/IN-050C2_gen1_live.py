import urllib.request, json
import csv,time
import datetime
import collections
import os
import re
import pytz


print('Running History script')
##with open('F:/Flexi_final/[IN-050C]/server_copy/IN-050C_gen1_history.py') as source_file: ###local
with open('/home/admin/CODE/DatabaseCreation/IN050C/IN-050C2_gen1_history.py') as source_file: ###server
        exec(source_file.read())
print('RETURNED Running Live script')

#Global declarations
wordDictI = {
'i1':'Id','i2':'Inverter_Id','i3':'Fb_Id','i4':'DTT','i5':'Device_Time','i6':'SES','i7':'Salve_Id','i8':'Function_Code','i9':'AC_Voltage_Line1','i10':'AC_Voltage_Line2',
'i11':'AC_Voltage_Line3','i12':'AC_Current_Line1','i13':'AC_Current_Line2','i14':'AC_Current_Line3','i15':'AC_Power','i16':'AC_Power_Percentage','i17':'AC_Frequency',
'i18':'Power_Factor','i19':'Reactive_Power','i20':'DC_Current','i21':'DC_Power','i22':'Inverter_Temprature','i23':'Time_Of_Use_Today','i24':'Time_Of_Use_Life',
'i25':'Energy_Produced','i26':'KWH_Counter','i27':'MWH_Counter','i28':'GWH_Counter','i29':'DC_Voltage','i30':'Inverter_Status','i31':'Energy_Generated_Today',
'i32':'Tstamp','i33':'Total_Energy_Generated_Till','i34':'Inverter_Communication_Status','i35':'AC_Power_2','i36':'AC_Power_3','i37':'AC_Frequency_2',
'i38':'AC_Frequency_3','i39':'DC_Voltage_2','i40':'DC_Power_2','i41':'DC_Current_2','i42':'Plant_Id','i43':'Inv_Status_Word','i44':'Grid_Conn_Status',
'i45':'AC_Current_Totoal','i46':'AC_Volatge_RY','i47':'AC_Volatge_YB','i48':'AC_Volatge_BR','i49':'Apparent_Power','i50':'Event_Flag_1',
'i51':'Event_Flag_2','i52':'Event_Flag_3','i53':'Event_Flag_4','i54':'Coolent_Temp'
}

wordDictM = {
'm8':'Voltage_R_Phase','m9':'Voltage_Y_Phase','m10':'Voltage_B_Phase','m11':'Average_Voltage','m12':'Voltage_R_Y','m13':'Voltage_Y_B','m14':'Voltage_B_R',
'm15':'Line_To_Line_Voltage_Average','m16':'Current_R','m17':'Current_Y','m18':'Current_B','m19':'Current_N','m20':'Average_Current','m21':'Frequency_R',
'm22':'Frequency_Y','m23':'Frequency_B','m24':'Average_Frequency','m25':'Power_Factor_R','m26':'Power_Factor_Y','m27':'Power_Factor_B','m28':'Average_Power_Factor',
'm29':'Active_Power_R','m30':'Active_Power_Y','m31':'Active_Power_B','m32':'Total_Power','m33':'Reactive_Power_R','m34':'Reactive_Power_Y','m35':'Reactive_Power_B',
'm36':'Total_Reactive_Power','m37':'Total_Apparent_Power','m38':'Active_Energy','m39':'Reactive_Energy','m40':'Apparent_Power_R','m41':'Apparent_Power_Y',
'm42':'Apparent_Power_B','m43':'Reactive_Energy2','m44':'Maximum_Active_Power','m45':'Minimum_Active_Power','m46':'Maximum_Reactive_Power','m47':'Minimum_Reactive_Power',
'm48':'Maximum_Apparent_Power','m49':'Wh_Received','m50':'VAh_Received','m51':'VARh_Inductive_Received','m52':'VARh_Capacitive_Received','m53':'Energy_Export1',
'm54':'VAh_Delivered','m55':'VARh_Ind_Delivered','m56':'VARh_Cap_Delivered','m57':'THD','m58':'Active_Total_Import','m59':'Active_Total_Export',
'm60':'Apparent_Import','m61':'Aparent_Export','m62':'Energy_Today','m63':'Tstamp','m64':'Energy_Import','m65':'Energy_Export','m67':'Total_KW_Avg'
 }

wordDictW = {
'w9':'Humidity_Min','w10':'Module_Temp1','w11':'Wind_Direction','w12':'Wind_Speed','w13':'Ambient_Temp','w14':'Module_Temp2_Actual','w15':'Humidity_Max',
'w16':'Humidity_Actual','w17':'Ambient_Temp_Min','w18':'Ambient_Temp_Max','w19':'Ambient_Temp_Avg','w20':'Global_Irradiation_Min','w21':'Irradiation_Tilt1_Actual',
'w22':'Irradiation_Tilt2_Actual','w23':'Tstamp','w24':'Global_Irradiation_Max','w25':'Global_Irradiation_Avg','w26':'Wind_Speed_Min','w27':'Wind_Speed_Max',
'w28':'Humidity_Avg','w29':'Wind_Direction_Min','w30':'Wind_Direction_Max','w31':'Wind_Speed_Avg','w32':'Global_Irradiation_Actual','w34':'Rain','w35':'Room_Temperature'
    }


last_record1 = last_record2 = last_record3 = last_record4 = last_record5 = last_record6 = last_record7 = last_record8 = 0
last_record9 = last_record10 = last_record11 = last_record12 = last_record13 = last_record14 = last_record15 = last_record16 = last_record17 = last_record18 = 0
last_record19 = last_record20 = last_record21 = last_record22 = last_record23 = last_record24 = last_record25 = last_record26 = last_record27 = last_record28 = 0
last_record29 = last_record30 = last_record31 = last_record32 = last_record33 = last_record34 = last_record35 = last_record36 = last_record37 = last_record38 = 0
last_record39 = last_record40 = last_record41 = last_record42 = last_record43 = last_record44 = last_record45 = last_record46 = last_record47 = last_record48 = 0
last_record49 = last_record50 = last_record51 = last_record52 = last_record53 = last_record54 = last_record55 = last_record56 = last_record57 = last_record58 = 0
last_record59 = last_record60 = last_record61 = last_record62 = last_record63 = last_record64 = last_record65 = last_record66 = last_record67 = last_record68 = 0
last_record69 = last_record70 = 0

tz = pytz.timezone('Asia/Kolkata')
today_date_1 = datetime.datetime.now(tz).date()

start_time1 = start_time2 = start_time3 = start_time4 = start_time5 = start_time6 = start_time7 = start_time8 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
start_time9 = start_time10 = start_time11 = start_time12 = start_time13 = start_time14 = start_time15 = start_time16 = start_time17 = start_time18 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
start_time19 = start_time20 = start_time21 = start_time22 = start_time23 = start_time24 = start_time25 = start_time26 = start_time27 = start_time28 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
start_time29 = start_time30 = start_time31 = start_time32 = start_time33 = start_time34 = start_time35 = start_time36 = start_time37 = start_time38 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
start_time39 = start_time40 = start_time41 = start_time42 = start_time43 = start_time44 = start_time45 = start_time46 = start_time47 = start_time48 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
start_time49 = start_time50 = start_time51 = start_time52 = start_time53 = start_time54 = start_time55 = start_time56 = start_time57 = start_time58 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
start_time59 = start_time60 = start_time61 = start_time62 = start_time63 = start_time64 = start_time65 = start_time66 = start_time67 = start_time68 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
start_time69 = start_time70 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")


last_date = datetime.datetime.now(tz).date()

dic = {}


#Function to determine the current day
def determine_date():
    global last_date    
    day_indicator = 0
    tz = pytz.timezone('Asia/Kolkata')
    today_date = datetime.datetime.now(tz).date()
    if today_date != last_date:
        last_date = today_date     
        day_indicator = 1        
    return day_indicator, today_date




#Function to determine path and create directory 
def determine_path(curr_date,device,fd):
    year = curr_date.strftime("%Y")
    month = curr_date.strftime("%Y-%m")
    day = curr_date.strftime("%Y-%m-%d")
##    master_path = 'F:/del/FlexiMC_Data/Gen1_Data/[IN-050C]/' ###local
    master_path = '/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-050C]/' ###server
    year_path = master_path+year+'/'
    month_path = year_path+month+'/'
    device_path = month_path+device+'/'
    final_path = device_path+'[IN-050C]-'+fd+'-'+day+'.txt'
    
    if not os.path.exists(year_path):
        os.makedirs(year_path)
    if not os.path.exists(month_path):
        os.makedirs(month_path)
    if not os.path.exists(device_path):
        os.makedirs(device_path)
    if first_iteration == 0:
        if os.path.exists(final_path):
            os.remove(final_path)
            print('[IN-050C]-'+fd+'-'+day+'.txt'+' removed (first)')
    
    return final_path


#Function to replace header names 
def multiple_replace(text,name_head):

  if name_head == 'inverter':
      for key in wordDictI:
          text = re.sub(r"\b%s\b" % key, wordDictI[key],text)
      return text

  if name_head == 'MFM':
      for key in wordDictM:
          text = re.sub(r"\b%s\b" % key, wordDictM[key],text)
      return text

  if name_head == 'WMS':
      for key in wordDictW:
          text = re.sub(r"\b%s\b" % key, wordDictW[key],text)
      return text
    

#Function to reorder columns 
def reorder(line,name_head):
    
    if name_head == 'inverter':
        col = 31
    elif name_head == 'MFM':
        col = 62
    else:
        col = 22
        
    split = line.split('\t')
    temp = split[0]
    split[0] = split[col]
    split[col] = temp
    join = '\t'.join(split)
    return join

#Function to join average line
def get_avg(start_time,no_col,value,name_head):
    avg_line = str(start_time)
    for i in range(1,no_col):
        avg_line = avg_line + '\t'+str(value[i])
    if name_head == 'inverter':
        avg_line = avg_line + '\n'
    return avg_line



#Function to compute average values
def cmpt_avg(no_col,value,occ,counter):
    for i in range(no_col):
        try:
            value[i] = float(value[i])
            if value[i]!= 0:
                if i == 32:
                    value[i] = value[i]/(occ-counter)
                else:
                    value[i] = value[i]/occ
                    value[i] = round(value[i],7)
        
        except ValueError:
            pass
    return value

#Function to update start file 
def update_start_file(curr_date):
##    file = open('F:/Flexi_final/[IN-050C]/Gen-1/IN050C2G.txt','w') ###local
    file = open('/home/admin/Start/IN050C2G.txt','w') ###server
    file.write(str(curr_date))
    file.close()
      
#Function to write files for each device
def meter(name_head,folder_name,file_field,last_record,today_date,start_time):
    
    update_start_file(today_date)
    write_path = determine_path(today_date,folder_name,file_field)
    read_path = write_path.replace('Dropbox/FlexiMC_Data/Gen1_Data', 'Data/Flexi_Raw_Data')
    try:
        if not os.path.exists(read_path):
                raise Exception('Empty')        
    except Exception as e:
            print('No file found - '+str(today_date)+' '+ folder_name)
            return last_record, start_time

    length = 0
    with open(read_path, 'r') as read_file, open(write_path, 'a') as write_file :
                print('reading '+ read_path)
                
                data = read_file.readlines()
                length = len(data)
                lines = []
                occ = 0
                counter = 0
                if last_record < length:
                    while last_record < length:
                        line = data[last_record]
                        line = reorder(line,name_head)
                        if last_record == 0:
                            line = multiple_replace(line,name_head)
                            lines.append(line)
                            last_record +=1
                        else:
                            split = line.split('\t')
                            no_col = len(split)
                            timestamp = split[0]
                            timestamp = timestamp[:16] #To remove seconds                        
                            curr_time = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M")

                            while start_time < curr_time:
                                if file_field in dic.keys():
                                    value = dic[file_field][0]
                                    occ = dic[file_field][1]
                                    counter = dic[file_field][2]
                                    dic.pop(file_field)
                                    value = cmpt_avg(no_col,value,occ,counter)
                                    if name_head == 'inverter':                                    
                                        if value[32] != 0:
                                            line = get_avg(start_time,no_col,value,name_head)
                                            lines.append(line)
                                            print('Buffer WRITTEN  ',start_time,write_path[-27:])
                                            print('last_record = '+str(last_record) + ' ' + 'length = '+str(length) + ' '+ 'start_time = '+str(start_time))
                                        else:
                                            print('Skipping ',str(start_time),' as Total Energy is 0')
                                    else:
                                        line = get_avg(start_time,no_col,value,name_head)
                                        lines.append(line)
                                        print('Buffer WRITTEN  ',start_time,write_path[-27:])
                                        print('last_record = '+str(last_record) + ' ' + 'length = '+str(length) + ' '+ 'start_time = '+str(start_time))
                                    
                                    
                                    occ = 0
                                    counter = 0
                                if occ>0:
                                    value = cmpt_avg(no_col,value,occ,counter)
                                    if name_head == 'inverter':                                    
                                        if value[32] != 0:
                                            line = get_avg(start_time,no_col,value,name_head)
                                            lines.append(line)
                                            print('WRITTEN ',start_time,write_path[-27:])
                                            print('last_record = '+str(last_record) + ' ' + 'length = '+str(length) + ' '+ 'start_time = '+str(start_time))
                                        else:
                                            print('Skipping ',str(start_time),' as Total Energy is 0')
                                    else:
                                        line = get_avg(start_time,no_col,value,name_head)
                                        lines.append(line)
                                        print('WRITTEN ',start_time,write_path[-27:])
                                        print('last_record = '+str(last_record) + ' ' + 'length = '+str(length) + ' '+ 'start_time = '+str(start_time))
                                     
                                start_time+=datetime.timedelta(minutes=5)
                                occ = 0
                                counter = 0

                            if occ == 0:
                                value = [0]*no_col
                            
                            if file_field in dic.keys():
                                value = dic[file_field][0]
                                occ = dic[file_field][1]
                                counter = dic[file_field][2]
                                dic.pop(file_field)
                            
                            if(curr_time <= start_time):
                                occ +=1
##                                print(str(curr_time)+' averaged ')
                                last_record +=1
                                for i in range(no_col):
                                    try:
                                        if name_head == 'inverter' and i == 32:
                                            if split[i] == '0' or split[i] == 'NA' or split[i] == 'NULL':
                                                counter += 1
                                        value[i] += float(split[i])
                                    except ValueError: ##to check if string
                                        value[i] = split[i]
                                    except TypeError:
                                        if value[i] == 'NULL' or value[i] == 'NA': ## If NULL/NA is first, then can't do 'value[i] += float(split[i])' operation
                                            value[i] = 0
                                            value[i] += float(split[i]) 
                            
                    ## to add remaining rows (needed for last record as it won't enter 'while start_time < curr_time:' loop as no values present after that)       
                    if occ>0 :
                        print('Waiting to check if new record falls within bucket range', 'lr= ',last_record,'len=', length,'cur= ',curr_time,'st= ',start_time)
##                        global dic
                        dic.update({file_field:[value,occ,counter]})
                            
                    write_file.writelines(lines)
                    return last_record, start_time
                

                else:
                    print('Waiting for new record '+read_path[-27:]+'last_record = '+str(last_record) + ' ' + 'length = '+str(length) + ' '+ 'start_time = '+str(start_time))
                    return last_record, start_time

def reset_all(today_date):
    global last_record1, last_record2, last_record3, last_record4, last_record5, last_record6, last_record7, last_record8, last_record9, last_record10
    global last_record11, last_record12, last_record13, last_record14, last_record15, last_record16, last_record17, last_record18, last_record19, last_record20
    global last_record21, last_record22, last_record23, last_record24, last_record25, last_record26, last_record27, last_record28, last_record29, last_record30
    global last_record31, last_record32, last_record33, last_record34, last_record35, last_record36, last_record37, last_record38, last_record39, last_record40
    global last_record41, last_record42, last_record43, last_record44, last_record45, last_record46, last_record47, last_record48, last_record49, last_record50
    global last_record51, last_record52, last_record53, last_record54, last_record55, last_record56, last_record57, last_record58, last_record59, last_record60
    global last_record61, last_record62, last_record63, last_record64, last_record65, last_record66, last_record67, last_record68, last_record69, last_record70

    global start_time1, start_time2, start_time3, start_time4, start_time5, start_time6, start_time7, start_time8, start_time9, start_time10
    global start_time11, start_time12, start_time13, start_time14, start_time15, start_time16, start_time17, start_time18, start_time19, start_time20
    global start_time21, start_time22, start_time23, start_time24, start_time25, start_time26, start_time27, start_time28, start_time29, start_time30
    global start_time31, start_time32, start_time33, start_time34, start_time35, start_time36, start_time37, start_time38, start_time39, start_time40
    global start_time41, start_time42, start_time43, start_time44, start_time45, start_time46, start_time47, start_time48, start_time49, start_time50
    global start_time51, start_time52, start_time53, start_time54, start_time55, start_time56, start_time57, start_time58, start_time59, start_time60
    global start_time61, start_time62, start_time63, start_time64, start_time65, start_time66, start_time67, start_time68, start_time69, start_time70


    last_record1 = last_record2 = last_record3 = last_record4 = last_record5 = last_record6 = last_record7 = last_record8 = 0
    last_record9 = last_record10 = last_record11 = last_record12 = last_record13 = last_record14 = last_record15 = last_record16 = last_record17 = last_record18 = 0
    last_record19 = last_record20 = last_record21 = last_record22 = last_record23 = last_record24 = last_record25 = last_record26 = last_record27 = last_record28 = 0
    last_record29 = last_record30 = last_record31 = last_record32 = last_record33 = last_record34 = last_record35 = last_record36 = last_record37 = last_record38 = 0
    last_record39 = last_record40 = last_record41 = last_record42 = last_record43 = last_record44 = last_record45 = last_record46 = last_record47 = last_record48 = 0
    last_record49 = last_record50 = last_record51 = last_record52 = last_record53 = last_record54 = last_record55 = last_record56 = last_record57 = last_record58 = 0
    last_record59 = last_record60 = last_record61 = last_record62 = last_record63 = last_record64 = last_record65 = last_record66 = last_record67 = last_record68 = 0
    last_record69 = last_record70 = 0

    start_time1 = start_time2 = start_time3 = start_time4 = start_time5 = start_time6 = start_time7 = start_time8 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
    start_time9 = start_time10 = start_time11 = start_time12 = start_time13 = start_time14 = start_time15 = start_time16 = start_time17 = start_time18 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
    start_time19 = start_time20 = start_time21 = start_time22 = start_time23 = start_time24 = start_time25 = start_time26 = start_time27 = start_time28 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
    start_time29 = start_time30 = start_time31 = start_time32 = start_time33 = start_time34 = start_time35 = start_time36 = start_time37 = start_time38 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
    start_time39 = start_time40 = start_time41 = start_time42 = start_time43 = start_time44 = start_time45 = start_time46 = start_time47 = start_time48 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
    start_time49 = start_time50 = start_time51 = start_time52 = start_time53 = start_time54 = start_time55 = start_time56 = start_time57 = start_time58 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
    start_time59 = start_time60 = start_time61 = start_time62 = start_time63 = start_time64 = start_time65 = start_time66 = start_time67 = start_time68 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
    start_time69 = start_time70 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")

    dic.clear()
    
    print('New day; Last record counters and start time reset')
    print('New Day sleeping for 27')
    time.sleep(1620)

        

first_iteration = 0
while True:
    day_indicator, today_date = determine_date()

    if day_indicator == 1:
        reset_all(today_date)

    
    last_record1, start_time1 = meter('inverter','ACH_Inverter_1','ACH_I1',last_record1,today_date,start_time1)
    last_record2, start_time2 = meter('inverter','ACH_Inverter_2','ACH_I2',last_record2,today_date,start_time2)
    last_record3, start_time3 = meter('inverter','ACH_Inverter_3','ACH_I3',last_record3,today_date,start_time3)
    last_record4, start_time4 = meter('inverter','ACH_Inverter_4','ACH_I4',last_record4,today_date,start_time4)
    last_record5, start_time5 = meter('inverter','ACH_Inverter_5','ACH_I5',last_record5,today_date,start_time5)
    last_record6, start_time6 = meter('inverter','ACH_Inverter_6','ACH_I6',last_record6,today_date,start_time6)
    last_record7, start_time7 = meter('inverter','ACH_Inverter_7','ACH_I7',last_record7,today_date,start_time7)
    last_record8, start_time8 = meter('inverter','ACH_Inverter_8','ACH_I8',last_record8,today_date,start_time8)
    last_record9, start_time9 = meter('inverter','ACH_Inverter_9','ACH_I9',last_record9,today_date,start_time9)
    last_record10, start_time10 = meter('inverter','ANM_Inverter_1','ANM_I1',last_record10,today_date,start_time10)
    last_record11, start_time11 = meter('inverter','ANM_Inverter_2','ANM_I2',last_record11,today_date,start_time11)
    last_record12, start_time12 = meter('inverter','ANM_Inverter_3','ANM_I3',last_record12,today_date,start_time12)
    last_record13, start_time13 = meter('inverter','ANM_Inverter_4','ANM_I4',last_record13,today_date,start_time13)
    last_record14, start_time14 = meter('inverter','ANM_Inverter_5','ANM_I5',last_record14,today_date,start_time14)
    last_record15, start_time15 = meter('inverter','ANM_Inverter_6','ANM_I6',last_record15,today_date,start_time15)
    last_record16, start_time16 = meter('inverter','ANM_Inverter_7','ANM_I7',last_record16,today_date,start_time16)
    last_record17, start_time17 = meter('inverter','ANM_Inverter_8','ANM_I8',last_record17,today_date,start_time17)
    last_record18, start_time18 = meter('inverter','ANM_Inverter_9','ANM_I9',last_record18,today_date,start_time18)
    last_record19, start_time19 = meter('inverter','ACH_Inverter_10','ACH_I10',last_record19,today_date,start_time19)
    last_record20, start_time20 = meter('inverter','ACH_Inverter_11','ACH_I11',last_record20,today_date,start_time20)
    last_record21, start_time21 = meter('inverter','ACH_Inverter_12','ACH_I12',last_record21,today_date,start_time21)
    last_record22, start_time22 = meter('inverter','ACH_Inverter_13','ACH_I13',last_record22,today_date,start_time22)
    last_record23, start_time23 = meter('inverter','ACH_Inverter_14','ACH_I14',last_record23,today_date,start_time23)
    last_record24, start_time24 = meter('inverter','ACH_Inverter_15','ACH_I15',last_record24,today_date,start_time24)
    last_record25, start_time25 = meter('inverter','ACH_Inverter_16','ACH_I16',last_record25,today_date,start_time25)
    last_record26, start_time26 = meter('inverter','ACH_Inverter_17','ACH_I17',last_record26,today_date,start_time26)
    last_record27, start_time27 = meter('inverter','ACH_Inverter_18','ACH_I18',last_record27,today_date,start_time27)
    last_record28, start_time28 = meter('inverter','ACH_Inverter_19','ACH_I19',last_record28,today_date,start_time28)
    last_record29, start_time29 = meter('inverter','ACH_Inverter_20','ACH_I20',last_record29,today_date,start_time29)
    last_record30, start_time30 = meter('inverter','ACH_Inverter_21','ACH_I21',last_record30,today_date,start_time30)
    last_record31, start_time31 = meter('inverter','ANM_Inverter_10','ANM_I10',last_record31,today_date,start_time31)
    last_record32, start_time32 = meter('inverter','ANM_Inverter_11','ANM_I11',last_record32,today_date,start_time32)
    last_record33, start_time33 = meter('inverter','ANM_Inverter_12','ANM_I12',last_record33,today_date,start_time33)
    last_record34, start_time34 = meter('inverter','ANM_Inverter_13','ANM_I13',last_record34,today_date,start_time34)
    last_record35, start_time35 = meter('inverter','ANM_Inverter_14','ANM_I14',last_record35,today_date,start_time35)
    last_record36, start_time36 = meter('inverter','ANM_Inverter_15','ANM_I15',last_record36,today_date,start_time36)
    last_record37, start_time37 = meter('inverter','ANM_Inverter_16','ANM_I16',last_record37,today_date,start_time37)
    last_record38, start_time38 = meter('inverter','ANM_Inverter_17','ANM_I17',last_record38,today_date,start_time38)
    last_record39, start_time39 = meter('inverter','ANM_Inverter_18','ANM_I18',last_record39,today_date,start_time39)
    last_record40, start_time40 = meter('inverter','ANM_Inverter_19','ANM_I19',last_record40,today_date,start_time40)
    last_record41, start_time41 = meter('inverter','ANM_Inverter_20','ANM_I20',last_record41,today_date,start_time41)
    last_record42, start_time42 = meter('inverter','ANM_Inverter_21','ANM_21',last_record42,today_date,start_time42)
    last_record43, start_time43 = meter('inverter','ANK_Inverter_1','ANK_I1',last_record43,today_date,start_time43)
    last_record44, start_time44 = meter('inverter','ANK_Inverter_2','ANK_I2',last_record44,today_date,start_time44)
    last_record45, start_time45 = meter('inverter','ANK_Inverter_3','ANK_I3',last_record45,today_date,start_time45)
    last_record46, start_time46 = meter('inverter','ANK_Inverter_4','ANK_I4',last_record46,today_date,start_time46)
    last_record47, start_time47 = meter('inverter','ANK_Inverter_5','ANK_I5',last_record47,today_date,start_time47)
    last_record48, start_time48 = meter('inverter','ANK_Inverter_6','ANK_I6',last_record48,today_date,start_time48)
    last_record49, start_time49 = meter('inverter','ANK_Inverter_7','ANK_I7',last_record49,today_date,start_time49)
    last_record50, start_time50 = meter('inverter','ANK_Inverter_8','ANK_I8',last_record50,today_date,start_time50)
    last_record51, start_time51 = meter('inverter','ANK_Inverter_9','ANK_I9',last_record51,today_date,start_time51)
    last_record52, start_time52 = meter('inverter','ANK_Inverter_10','ANK_I10',last_record52,today_date,start_time52)
    last_record53, start_time53 = meter('inverter','ANK_Inverter_11','ANK_I11',last_record53,today_date,start_time53)
    last_record54, start_time54 = meter('inverter','ANK_Inverter_12','ANK_I12',last_record54,today_date,start_time54)
    last_record55, start_time55 = meter('inverter','TP1_Inverter_1','TP1_I1',last_record55,today_date,start_time55)
    last_record56, start_time56 = meter('inverter','TP1_Inverter_2','TP1_I2',last_record56,today_date,start_time56)
    last_record57, start_time57 = meter('inverter','TP1_Inverter_3','TP1_I3',last_record57,today_date,start_time57)
    last_record58, start_time58 = meter('inverter','TP1_Inverter_4','TP1_I4',last_record58,today_date,start_time58)
    last_record59, start_time59 = meter('inverter','TP2_Inverter_1','TP2_I1',last_record59,today_date,start_time59)
    last_record60, start_time60 = meter('inverter','TP2_Inverter_2','TP2_I2',last_record60,today_date,start_time60)
    last_record61, start_time61 = meter('inverter','TP2_Inverter_3','TP2_I3',last_record61,today_date,start_time61)
    last_record62, start_time62 = meter('inverter','TP2_Inverter_4','TP2_I4',last_record62,today_date,start_time62)
    last_record63, start_time63 = meter('MFM','TP1_MFM1','TP1_MFM1',last_record63,today_date,start_time63)
    last_record64, start_time64 = meter('MFM','TP2_MFM1','TP2_MFM1',last_record64,today_date,start_time64)
    last_record65, start_time65 = meter('MFM','ACH_MFM1','ACH_MFM1',last_record65,today_date,start_time65)
    last_record66, start_time66 = meter('MFM','ACH_MFM2','ACH_MFM2',last_record66,today_date,start_time66)
    last_record67, start_time67 = meter('MFM','ANK_MFM1','ANK_MFM1',last_record67,today_date,start_time67)
    last_record68, start_time68 = meter('MFM','ANM_MFM1','ANM_MFM1',last_record68,today_date,start_time68)
    last_record69, start_time69 = meter('MFM','ANM_MFM2','ANM_MFM2',last_record69,today_date,start_time69)
    last_record70, start_time70 = meter('WMS','ARV_WMS','ARV_WMS',last_record70,today_date,start_time70)    
    
    first_iteration = 1
    time.sleep(180)



        

    
