import urllib.request, json
import csv,time
import datetime
import collections
import os
import pytz
import smtplib
import ssl

ssl._create_default_https_context = ssl._create_unverified_context
##import logging

print('Running History script')
##with open('F:/Flexi_final/[IN-036C]/Raw/IN-036C_raw_history.py') as source_file: ###local
with open('/home/admin/CODE/DatabaseCreation/IN036C/IN-036C_raw_history.py') as source_file: ###server
        exec(source_file.read())
print('RETURNED Running Live script')

##logging.basicConfig(filename = '/home/admin/CODE/DatabaseCreation/IN036C/logger.txt', level = logging.INFO)
##logging.basicConfig(filename = 'F:/Flexi_final/[IN-036C]/logger.txt', level = logging.INFO)

#global declarations
ihead = ['i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20',
'i21','i22','i23','i24','i25','i26','i27','i28','i29','i30','i31','i32','i33','i34','i35','i36','i37','i38','i39','i40',
'i41','i42','i43','i44','i45','i46','i47','i48','i49','i50','i51','i52','i53','i54','i55','i56','i57','i58','i59','i60',
'i61','i62','i63','i64','i65','i66','i67','i68','i69','i70','i71','i72','i73','i74','i75','i76','i77','i78','i79','i80',
'i81','i82','i83','i84','i85','i86','i87']

mhead = ['m1','m2','m3','m4','m5','m6','m7','m8','m9','m10','m11','m12','m13','m14','m15','m16','m17','m18','m19','m20',
'm21','m22','m23','m24','m25','m26','m27','m28','m29','m30','m31','m32','m33','m34','m35','m36','m37','m38','m39','m40',
'm41','m42','m43','m44','m45','m46','m47','m48','m49','m50','m51','m52','m53','m54','m55','m56','m57','m58','m59','m60',
'm61','m62','m63','m64','m65','m66','m67']

whead = ['w1','w2','w3','w4','w5','w6','w7','w8','w9','w10','w11','w12','w13','w14','w15','w16','w17','w18','w19','w20','w21','w22','w23',
'w24','w25','w26','w27','w28','w29','w30','w31','w32','w33','w34','w35']


last_record1 = last_record2 = last_record3 = last_record4 = last_record5 = last_record6 = last_record7 = last_record8 = last_record9 = last_record10 = 0
last_record11 = last_record12 = last_record13 = last_record14 = last_record15 = last_record16 = last_record17 = last_record18 = last_record19 = last_record20 = 0
last_record21 = last_record22 = last_record23 = last_record24 = last_record25 = last_record26 = last_record27 = last_record28 = last_record29 = last_record30 = 0
last_record31 = last_record32 = last_record33 = last_record34 = last_record35 = last_record36 = last_record37 = last_record38 = last_record39 = last_record40 = 0
last_record41 = last_record42 = last_record43 = last_record44 = last_record45 = last_record46 = last_record47 = last_record48 = last_record49 = last_record50 = 0
last_record51 = last_record52 = last_record53 = last_record54 = 0

tz = pytz.timezone('Asia/Kolkata')
last_date = datetime.datetime.now(tz).date()


#Function to determine the current day
def determine_date():
    global last_date
    global last_record1, last_record2, last_record3, last_record4, last_record5, last_record6, last_record7, last_record8, last_record9, last_record10
    global last_record11, last_record12, last_record13, last_record14, last_record15, last_record16, last_record17, last_record18, last_record19, last_record20 
    global last_record21, last_record22, last_record23, last_record24, last_record25, last_record26, last_record27, last_record28, last_record29, last_record30
    global last_record31, last_record32, last_record33, last_record34, last_record35, last_record36, last_record37, last_record38, last_record39, last_record40 
    global last_record41, last_record42, last_record43, last_record44, last_record45, last_record46, last_record47, last_record48, last_record49, last_record50
    global last_record51, last_record52, last_record53, last_record54
    
    tz = pytz.timezone('Asia/Kolkata')
    today_date = datetime.datetime.now(tz).date() 
    
    if today_date != last_date:
        last_date = today_date
        print('*********NEW DAY**********')
        last_record1 = last_record2 = last_record3 = last_record4 = last_record5 = last_record6 = last_record7 = last_record8 = last_record9 = last_record10 = 0
        last_record11 = last_record12 = last_record13 = last_record14 = last_record15 = last_record16 = last_record17 = last_record18 = last_record19 = last_record20 = 0
        last_record21 = last_record22 = last_record23 = last_record24 = last_record25 = last_record26 = last_record27 = last_record28 = last_record29 = last_record30 = 0
        last_record31 = last_record32 = last_record33 = last_record34 = last_record35 = last_record36 = last_record37 = last_record38 = last_record39 = last_record40 = 0
        last_record41 = last_record42 = last_record43 = last_record44 = last_record45 = last_record46 = last_record47 = last_record48 = last_record49 = last_record50 = 0
        last_record51 = last_record52 = last_record53 = last_record54 = 0
        print('New day; Last record counters reset')
        print('New day sleeping for 20')
        time.sleep(1200)
        
    return today_date


#Function to determine URL for the next day
def determine_url(date,url_head):
    replace_date = date.strftime("%Y%m%d")
    url = 'http://13.127.216.107/API/JSON/swl/Bescom/Data/'+replace_date+'/'+url_head+replace_date+'.json'
    return url


#Function to determine path and create directory 
def determine_path(curr_date,device,fd):
    year = curr_date.strftime("%Y")
    month = curr_date.strftime("%Y-%m")
    day = curr_date.strftime("%Y-%m-%d")
##    master_path = 'F:/del/FlexiMC_Data/Raw_Data/[IN-036C]/' ###local
    master_path = '/home/admin/Data/Flexi_Raw_Data/[IN-036C]/' ###server
    year_path = master_path+year+'/'
    month_path = year_path+month+'/'
    device_path = month_path+device+'/'
    final_path = device_path+'[IN-036C]-'+fd+'-'+day+'.txt'
    
    if not os.path.exists(year_path):
        os.makedirs(year_path)
    if not os.path.exists(month_path):
        os.makedirs(month_path)
    if not os.path.exists(device_path):
        os.makedirs(device_path)
    if first_iteration == 0:
        if os.path.exists(final_path):
            os.remove(final_path)
            print('[IN-036C]-'+fd+'-'+day+'.txt'+' removed (first)')
    
    return final_path


#Function to update start file 
def update_start_file(curr_date):
##    file = open('F:/Flexi_final/[IN-036C]/Raw/IN036C.txt','w') ###local
    file = open('/home/admin/Start/IN036C.txt','w') ###server
    file.write(str(curr_date))
    file.close()


#Function to send mail
def sendmail():
    fromaddr = 'operations@cleantechsolar.com'
    toaddr = 'kn.rohan@gmail.com','andre.nobre@cleantechsolar.com','shravan1994@gmail.com','rupesh.baker@cleantechsolar.com'
    uname = 'shravan.karthik@cleantechsolar.com'

    subject = 'Bot IN-036C Raw Down - Portal Error'
    text = 'B '
    message = 'Subject: {}\n\n{}'.format(subject,text)
    password = 'CTS&*(789'
    try:
        print('send try')
        server = smtplib.SMTP('smtp-mail.outlook.com',587)
        server.ehlo()
        server.starttls()
        server.login(uname,password)
        server.sendmail(fromaddr,toaddr,message)
        print('sending')
        server.quit()
    except Exception as e:
        print('mail send exception - ',str(e))



#Function to get header names
def get_header(json_head):
    if json_head == 'inverter':
        return ihead
    elif json_head == 'MFM':
        return mhead
    else:
        return whead

#Function to determine timestamp for logs
def determine_log_timestamp(json_head,data):
    if json_head == 'inverter':
        timestamp = data['i32']
    elif json_head == 'MFM':
        timestamp = data['m64']
    else:
        timestamp = data['w23']
    return timestamp


#Function to re-try connection
def connection_try(url):
    counter = 0
    while True:
        counter += 1
        try:
            print('second attempt try - ',counter)
            with urllib.request.urlopen(url) as url:
                try_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
                return
        except Exception as e:
            if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                if counter == 4:
                    sendmail()
                    exit('Exiting due to persistent connection error')
                    
                print('sleeping for 15')
                time.sleep(900)
            else:
                return

#Function to write files for each device
def meter(url_head,json_head,folder_name,file_field,last_record,today_date):
    
    length = 0
    url = determine_url(today_date,url_head)
    path = determine_path(today_date,folder_name,file_field)
    try:
        with urllib.request.urlopen(url) as url:
            load_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
            update_start_file(today_date)
            
    except Exception as e:
            if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                print('Incomplete read/connection refuse error - 1  Slepping for 15 min - ',str(e))
                time.sleep(900)
                connection_try(url)
                try:
                    print('Last attempt try')
                    with urllib.request.urlopen(url) as url:
                        load_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
                        update_start_file(today_date)
                except Exception as e:
                    if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                        print('Incomplete read/connection refuse error - 2  Killing bot and sending mail - ',str(e))
                        sendmail()
                        exit('Exiting due to persistent connection error')
                        
                    else:
                        print('No data found - '+str(today_date)+' '+ folder_name+' '+str(e))
            ##            logging.exception(str(e))
                        return last_record
            else:
                print('No data found - '+str(today_date)+' '+ folder_name+' '+str(e))
    ##            logging.exception(str(e))
                return last_record

    if load_data[json_head] ==None:
        print('NULL Returned by Flexi')
        return last_record
        
    length = len(load_data[json_head])
    
    if last_record < length:            
        while last_record < length:
            data = load_data[json_head][last_record]
            timestamp = determine_log_timestamp(json_head,data)
            with open(path, 'a',newline='') as csv_file:
                csvwriter = csv.writer(csv_file, delimiter='\t')
                line = []
                header = []
                
                header = get_header(json_head)
                line = [data.get(k,'NA') for k in header]
                line = ['NULL' if w is None else w for w in line]

                if last_record == 0:
                    csvwriter.writerow(header)
                    
                csvwriter.writerow(line)
                print(timestamp +' written '+path[-26:])
                csv_file.close()
            last_record += 1
            print('last_record = '+str(last_record) + ' ' + 'length = '+str(length))
            
        return last_record
    else:
        print('Waiting for new record '+path[-26:]+'last_record = '+str(last_record) + ' ' + 'length = '+str(length))
        return last_record


first_iteration = 0
while True:
    today_date = determine_date()
    last_record1 = meter('22_Inverter_1_','inverter','C_Inverter_1','CI1',last_record1,today_date)
    last_record2 = meter('22_Inverter_2_','inverter','C_Inverter_2','CI2',last_record2,today_date)
    
    last_record3 = meter('22_Inverter_3_','inverter','S_Inverter_1','SI1',last_record3,today_date)
    last_record4 = meter('22_Inverter_4_','inverter','S_Inverter_2','SI2',last_record4,today_date)
    last_record5 = meter('22_Inverter_5_','inverter','S_Inverter_3','SI3',last_record5,today_date) 
    last_record6 = meter('22_Inverter_6_','inverter','S_Inverter_4','SI4',last_record6,today_date)
    last_record7 = meter('22_Inverter_7_','inverter','S_Inverter_5','SI5',last_record7,today_date)
    last_record8 = meter('22_Inverter_8_','inverter','S_Inverter_6','SI6',last_record8,today_date)
    last_record9 = meter('22_Inverter_9_','inverter','S_Inverter_7','SI7',last_record9,today_date) 
    last_record10 = meter('22_Inverter_10_','inverter','S_Inverter_8','SI8',last_record10,today_date)
    last_record11 = meter('22_Inverter_11_','inverter','S_Inverter_9','SI9',last_record11,today_date)
    last_record12 = meter('22_Inverter_12_','inverter','S_Inverter_10','SI10',last_record12,today_date)
    last_record13 = meter('22_Inverter_13_','inverter','S_Inverter_11','SI11',last_record13,today_date)
    last_record14 = meter('22_Inverter_14_','inverter','S_Inverter_12','SI12',last_record14,today_date)
    last_record15 = meter('22_Inverter_15_','inverter','S_Inverter_13','SI13',last_record15,today_date)
    last_record16 = meter('22_Inverter_16_','inverter','S_Inverter_14','SI14',last_record16,today_date)
    last_record17 = meter('22_Inverter_17_','inverter','S_Inverter_15','SI15',last_record17,today_date)
    last_record18 = meter('22_Inverter_18_','inverter','S_Inverter_16','SI16',last_record18,today_date)
    last_record19 = meter('22_Inverter_19_','inverter','S_Inverter_17','SI17',last_record19,today_date)
    last_record20 = meter('22_Inverter_20_','inverter','S_Inverter_18','SI18',last_record20,today_date)
    last_record21 = meter('22_Inverter_21_','inverter','S_Inverter_19','SI19',last_record21,today_date) 
    last_record22 = meter('22_Inverter_22_','inverter','S_Inverter_20','SI20',last_record22,today_date)
    last_record23 = meter('22_Inverter_23_','inverter','S_Inverter_21','SI21',last_record23,today_date)
    last_record24 = meter('22_Inverter_24_','inverter','S_Inverter_22','SI22',last_record24,today_date)
    last_record25 = meter('22_Inverter_25_','inverter','S_Inverter_23','SI23',last_record25,today_date)    
    last_record26 = meter('22_Inverter_26_','inverter','S_Inverter_24','SI24',last_record26,today_date)
    last_record27 = meter('22_Inverter_27_','inverter','S_Inverter_25','SI25',last_record27,today_date)
    last_record28 = meter('22_Inverter_28_','inverter','S_Inverter_26','SI26',last_record28,today_date)
    last_record29 = meter('22_Inverter_29_','inverter','S_Inverter_27','SI27',last_record29,today_date)    
    last_record30 = meter('22_Inverter_30_','inverter','S_Inverter_28','SI28',last_record30,today_date)
    last_record31 = meter('22_Inverter_31_','inverter','S_Inverter_29','SI29',last_record31,today_date) 
    last_record32 = meter('22_Inverter_32_','inverter','S_Inverter_30','SI30',last_record32,today_date)
    last_record33 = meter('22_Inverter_33_','inverter','S_Inverter_31','SI31',last_record33,today_date)
    last_record34 = meter('22_Inverter_34_','inverter','S_Inverter_32','SI32',last_record34,today_date)
    last_record35 = meter('22_Inverter_35_','inverter','S_Inverter_33','SI33',last_record35,today_date)    
    last_record36 = meter('22_Inverter_36_','inverter','S_Inverter_34','SI34',last_record36,today_date)
    last_record37 = meter('22_Inverter_37_','inverter','S_Inverter_35','SI35',last_record37,today_date)
    last_record38 = meter('22_Inverter_38_','inverter','S_Inverter_36','SI36',last_record38,today_date)
    last_record39 = meter('22_Inverter_39_','inverter','S_Inverter_37','SI37',last_record39,today_date)    
    last_record40 = meter('22_Inverter_40_','inverter','S_Inverter_38','SI38',last_record40,today_date)
    last_record41 = meter('22_Inverter_41_','inverter','S_Inverter_39','SI39',last_record41,today_date) 
    last_record42 = meter('22_Inverter_42_','inverter','S_Inverter_40','SI40',last_record42,today_date)
    last_record43 = meter('22_Inverter_43_','inverter','S_Inverter_41','SI41',last_record43,today_date)
    last_record44 = meter('22_Inverter_44_','inverter','S_Inverter_42','SI42',last_record44,today_date)
    last_record45 = meter('22_Inverter_45_','inverter','S_Inverter_43','SI43',last_record45,today_date)    
    last_record46 = meter('22_Inverter_46_','inverter','S_Inverter_44','SI44',last_record46,today_date)            
    last_record47 = meter('22_Inverter_47_','inverter','S_Inverter_45','SI45',last_record47,today_date)
    last_record48 = meter('22_Inverter_48_','inverter','S_Inverter_46','SI46',last_record48,today_date)
    last_record49 = meter('22_Inverter_49_','inverter','S_Inverter_47','SI47',last_record49,today_date)
    
    last_record50 = meter('22_MFM_4_','MFM','LT','LT',last_record50,today_date)    
    last_record51 = meter('22_MFM_3_','MFM','ACDB','ACDB',last_record51,today_date)
    last_record52 = meter('22_MFM_1_','MFM','SI_MFM','SI_MFM',last_record52,today_date) 
    last_record53 = meter('22_MFM_2_','MFM','CI_MFM','CI_MFM',last_record53,today_date)
    
    last_record54 = meter('22_WMS_1_','WMS','WMS','WMS',last_record54,today_date)
    
    first_iteration = 1
    time.sleep(180)






        

    
