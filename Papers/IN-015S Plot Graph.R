rm(list=ls(all =TRUE))

library(ggplot2)
library(reshape)
library(lubridate)
library(devtools)

pathWrite <- "~/Graphs/"
result <- read.csv("/tmp/IN-015S_PR_summary.csv",stringsAsFactors = F)

rownames(result) <- NULL
result <- data.frame(result)
result$Date <- as.POSIXct(result$Date, format = "%Y-%m-%d")
result$Color <- factor(result$Color,levels = c("0","1","2"), labels = c("0","1","2"))

res2y = as.numeric(result[(as.character(result[,4]) == "2"),3])
res2x = as.numeric(result[(as.character(result[,4]) == "2"),5])
lm(res2y ~ res2x)
avg = mean(result$PR,na.rm=TRUE)
std = sd(result$PR,na.rm=TRUE)
sigma2POS = avg + (2*std)
sigma2NEG = avg - (2*std)

No_of_points <- nrow(result)

idxuse = which(as.character(result$Color) %in%  "1")
idxuse2 = which(as.character(result$Color) %in% "2")
idxuse = c(idxuse,idxuse2)
m <- lm(result$PR[idxuse] ~ as.Date(result$Date[idxuse]))
a <- signif(coef(m)[1], digits = 2)
b <- signif(coef(m)[2], digits = 2)
equation <- paste("y = ",a," +", b," x", sep="")


PRGraph <- ggplot(data=result,aes(x=as.Date(Date),y=PR,color=Color,shape=Color)) + geom_point() + theme_classic()+
  geom_abline(intercept=a,slope=b,color="blue") +
  scale_y_continuous(limits = c(00, 110), expand= c(0,0), breaks=seq(0,100,10)) +
 	scale_x_date(date_breaks = "3 months",date_labels = "%b/%y") + 
#	scale_x_continuous(limits = c(0, 1000), expand= c(0,0),breaks=seq(0,1000,100)) +
#  ggtitle(expression(bold("[IN-015S] Lifetime Performance Ratio (without 2"*sigma~"filter)")), subtitle = paste0("From ",result$Date[1]," to ",result$Date[nrow(result)])) +
  theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
        plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5),
				legend.justification = c(0.5, 0.97), legend.position = c(0.5, 1),legend.background = element_rect(fill=alpha('blue', 0))) +
  ylab("Performance Ratio [%]") + 
  xlab("") +
	annotate("text",label="Heavy",x = as.Date(result$Date[280]), y= 62.5,fontface =1,hjust = 0) + 
	annotate("text",label="Soiling",x = as.Date(result$Date[280]), y= 59,fontface =1,hjust = 0) + 
	annotate("text",label="Heavy",x = as.Date(result$Date[880]), y= 66.5,fontface =1,hjust = 0) + 
	annotate("text",label="Soiling",x = as.Date(result$Date[880]), y= 63,fontface =1,hjust = 0) + 
	annotate("text",label="Heavy",x = as.Date(result$Date[50]), y= 62.5,fontface =1,hjust = 0) + 
	annotate("text",label="Soiling",x = as.Date(result$Date[50]), y= 59,fontface =1,hjust = 0) + 
	annotate("text",label="Heavy",x = as.Date(result$Date[750]), y= 69.5,fontface =1,hjust = 0) + 
	annotate("text",label="Soiling",x = as.Date(result$Date[750]), y= 66,fontface =1,hjust = 0) + 
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
 # geom_hline(yintercept=sigma2POS, linetype="dashed") +
 # geom_hline(yintercept=sigma2NEG, linetype="dashed") + 
 # annotate("text",label = paste(expression(+2*sigma ~ " [96.2%]")),size = 3,parse=T,  #update the value(%) according to Sigma2POS
  #         x = as.Date(result$Date[50]), y= sigma2POS+2.5,fontface =1,hjust = 0) +
  #annotate("text",label = paste(expression(-2*sigma ~ " [54.1%]")),size = 3,parse=T,  #update the value(%) according to Sigma2NEG
   #        x = as.Date(result$Date[50]), y= sigma2NEG-2.5,fontface =1,hjust = 0) +
#  annotate("text",label = paste0("Avg PR = ",sprintf("%0.1f", avg)," %"),size = 3,
#           x = as.Date(result$Date[650]), y= 18.5
#           ,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Degradation = ", sprintf("%0.1f", b*365) ," % p.a"),size = 3,
           x=as.Date(result$Date[400]), y= 27,fontface =1,hjust = 0) +
#  annotate("text",label = paste0(equation),size = 3,
#           x = as.Date(result$Date[200]), y= 20,fontface =1,hjust = 0) +
#  annotate("text",label = paste0("Start PR = ",sprintf("%0.1f", a), " %"),size = 3,
#           x = as.Date(result$Date[200]), y= 30,fontface =1,hjust = 0) +
#  annotate("text",label = paste0("Current PR = ",sprintf("%0.1f", a+No_of_points*b)," %"),size = 3,
#           x = as.Date(result$Date[200]),y= 25,fontface =1,hjust = 0) +
  theme(plot.margin = unit(c(0.5,1.0,0.2,0.2),"cm"))  #top, right, bottom, left
p1 <- PRGraph
p1 <- p1 + theme(legend.text=element_text(size=12), legend.title=element_text(size=12))
p1 <- p1 + scale_colour_manual('', 
                               values = c("gray", "blue","green"), 
                               labels = c("Uncleaned System","First days after cleaning cycle","Rainy Months"), guide = guide_legend(title.position = "left",nrow=1)) 
p1 <- p1 + scale_shape_manual('',
                              labels = c("Uncleaned System","First days after cleaning cycle","Rainy Months"),
                              values = c(0,1,2), guide = guide_legend(title.position = "left",nrow=1))
p1 <- p1 + theme(legend.box = "horizontal",legend.direction="horizontal") #legend.position = "bottom")
#p1 = p1 + geom_abline(intercept = 80.52, slope = -0.0006202, color="green")
#p1 = p1 + 
#  annotate("text",label = paste("y = x*-0.0006 + 80.52"), x=as.Date(result$Date[800]), y= 72,fontface =1,hjust = 0) 

#p1 <- p1 + geom_abline(intercept=1600,slope=1,color="red")
#p1 <- p1 + geom_abline(intercept=1500,slope=0.9,color="red")
#p1 <- p1 + geom_abline(intercept=1400,slope=0.8,color="red")
#p1 <- p1 + geom_abline(intercept=1300,slope=0.7,color="red")
#p1 <- p1 + geom_abline(intercept=1200,slope=0.6,color="red")
#p1 <- p1 + geom_abline(intercept=1300,slope=1,color="red")

#p1 <- p1 + geom_hline(yintercept = 72,colour="darkgreen",size=1) 
#p1 <- p1 + annotate('text', label = "Target Budget Yield Performance Ratio [72.0%]", y = 74 , x = as.Date(dff[101,1]), colour = "darkgreen")
#p1 <- p1 + guides(colour = guide_legend(override.aes = list(shape = 18)))

#ggsave("/home/admin/Graphs/frau.pdf", p1, width = 11, height=8)
PRGraph <- p1

print("saving")
ggsave(PRGraph,filename = paste0(pathWrite,"IN-015S_lifetime_PR_Without_Filter.pdf"), width = 7.92, height = 5) 
