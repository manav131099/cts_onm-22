rm(list=ls(all =TRUE))

pathRead <- "/home/admin/Dropbox/Second Gen/[IN-015S]"
pathWrite <- "/tmp/IN-015S_PR_summary.txt"
pathWrite2 <- "/tmp/IN-015S_PR_summary.csv"
setwd(pathRead)                                     #set working directory
print("Extracting data..")
filelist <- dir(pattern = ".txt", recursive= TRUE)  #contains only files of '.txt' format

nameofStation <- "IN-015S"

col0 <- c()
col1 <- c()
col2 <- c()


col0[10^6] = col1[10^6] = col2[10^6] = 0
coln = c()
daysclean = c(
"2018-05-08",
"2018-04-16",
"2018-03-08",
"2018-02-08",
"2018-01-08",
"2017-12-11",
"2017-10-11",
"2017-07-03",
"2017-05-24",
"2017-05-05"
)

index <- 1
resetIdx <- 11
for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")  #reading text file at location filelist[i]
  
  if(identical(nchar(i), as.integer(31))){
    
    for (j in 1:nrow(temp)){
      
      col0[index] <- nameofStation
      
      col1[index] <- paste0(temp[j,1])
      
      col2[index] <- as.numeric(temp[j,17])
      
			coln[index] <- "0"
			mtch = match(as.character(temp[j,1]),daysclean)
			substrmon = as.numeric(substr(as.character(temp[j,1]),6,7))
			if((substrmon == 11 || substrmon == 12) && ( is.finite(col2[index]) && col2[index] > 65))
				coln[index] <- "2"
			if(resetIdx < 11 && resetIdx > 0)
			{
				if(col2[index] > 65)
      			coln[index] <- "1"
			}
			resetIdx <- resetIdx + 1
			{
			if(index < 15)
      	coln[index] <- "1"
      else if(is.finite(mtch))
				resetIdx <- -4
			}
      index <- index + 1
      
    }
    
    print(paste(i, "done"))
    
  }
  
}


col0 <- col0[1:(index-1)]   #removes last row for all columns
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
coln <- coln[1:(index-1)]
col1[is.na(col1)] <- NA              #states T/F
col2[is.na(col2)] <- NA


exclude_index <- 1
for(i in col2){
  if(isTRUE (i < 0)){  # eliminate PR below 0%
    col2[exclude_index] <- NA
  }
  
  if(isTRUE (i > 100)){   # eliminate PR above 100%
    col2[exclude_index] <- NA
  }
  exclude_index <- exclude_index +1
}


print("Starting to save..")

result <- cbind(col0,col1,col2,coln)
colnames(result) <- c("Meter Reference","Date","PR","Color")         #columns names


for(x in c(3)){       
  result[,x] <- round(as.numeric(result[,x]),1)
}

rownames(result) <- NULL
result <- data.frame(result) 
#result <- result[-c(1:120),] # remove row 1 to 120
for(i in 1:nrow(result)){
  result[i,5] <- i
}
colnames(result) <- c("Meter Reference","Date","PR","Color","No. of Days")
write.table(result,na = "",pathWrite,row.names = FALSE,sep ="\t")   #saving into txt and csv file
write.csv(result,pathWrite2, na= "", row.names = FALSE)
