rm(list = ls())
require("ggplot2")
getGIS = function(date)
{
	valret = NA
	path = "/home/admin/Dropbox/GIS/Summary/Chaksu/CHAK Aggregate.txt"
	data = read.table(path,header=T,sep="\t",stringsAsFactors=F)
	idx = match(date,as.character(data[,1]))
	if(is.finite(idx))
		valret = as.numeric(data[idx,2])
	return(valret)
}

path = "/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[IN-025C]/2018"
mons = dir(path)
c1 = c2 = c3 = c4 = c5 = c6 = c()
idx = 1

for( x in 1 : length(mons))
{
	pathmon = paste(path,mons[x],"WMS",sep = "/")
	days = dir(pathmon)
	for( y in 1 : length(days))
	{
		dataread = read.table(paste(pathmon,days[y],sep="/"),header=T,sep="\t",stringsAsFactors=F)
		c1[idx] = as.character(dataread[,1])
		c2[idx] = as.numeric(dataread[,2])
		c3[idx] = as.numeric(dataread[,3])
		c4[idx] = as.numeric(dataread[,8])
		if(c4[idx] >= -0.01 && c4[idx] < 0.01)
			c4[idx] = NA
		c5[idx] = round(c3[idx]/c4[idx],2)
		c6[idx] = getGIS(c1[idx])
		idx = idx + 1
	}
}
datawrite = data.frame(Date=c1,DA = c2, GTI = c3, DNI=c4,Ratio=c5,GHIGIS=c6,stringsAsFactors=F)

write.table(datawrite, file="/home/admin/Graphs/IN025Agg.txt",row.names=F,col.names=T,append=F,sep="\t")
meanda = round(mean(c2),1)
print(paste("Mean DA",meanda))

titlesettings <- theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
                  plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5))

p1 <- ggplot() + theme_bw()
p1 <- p1 + geom_line(data=datawrite, aes(x=as.Date(Date), y =DA), size = 1.5)
p1 <- p1 +  ylab("Data Availability [%]") + xlab("") + coord_cartesian(ylim = c(0,100))
p1 <- p1 + scale_x_date(date_breaks = "3 months",date_labels = "%b/%y") + scale_y_continuous(breaks=seq(0, 100, 10))
p1 <- p1 + theme(axis.title.x = element_text(size=13), axis.title.y = element_text(size=13),
                 axis.text = element_text(size=13), panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black")
                # ,legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97)
								 )                     #LEGNED AT RHS
p1 <- p1 + titlesettings + ggtitle('Data Availability - IN-025')
p1 <- p1 + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))
p1 <- p1 + annotate('text', label = paste("Avg DA: ",meanda,"%",sep=""), y = 20 , x = as.Date(datawrite[101,1]), colour = "red")

ggsave("/home/admin/Graphs/DA.pdf", p1, width = 11, height=8)

p1 <- ggplot() + theme_bw()
p1 <- p1 + geom_point(data=datawrite, aes(x=as.Date(Date), y =GTI), size = 1.5)
p1 <- p1 +  ylab("GTI [kWh/m2]") + xlab("") + coord_cartesian(ylim = c(0,10))
p1 <- p1 + scale_x_date(date_breaks = "3 months",date_labels = "%b/%y") + scale_y_continuous(breaks=seq(0, 10, 1))
p1 <- p1 + theme(axis.title.x = element_text(size=13), axis.title.y = element_text(size=13),
                 axis.text = element_text(size=13), panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black")
                # ,legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97)
								 )                     #LEGNED AT RHS
p1 <- p1 + titlesettings + ggtitle('GIT Irradiance - IN-025')
p1 <- p1 + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))

ggsave("/home/admin/Graphs/GTI.pdf", p1, width = 11, height=8)

p1 <- ggplot() + theme_bw()
p1 <- p1 + geom_point(data=datawrite, aes(x=as.Date(Date), y =DNI), size = 1.5)
p1 <- p1 +  ylab("DNI [kWh/m2]") + xlab("") + coord_cartesian(ylim = c(0,10))
p1 <- p1 + scale_x_date(date_breaks = "3 months",date_labels = "%b/%y") + scale_y_continuous(breaks=seq(0, 10, 1))
p1 <- p1 + theme(axis.title.x = element_text(size=13), axis.title.y = element_text(size=13),
                 axis.text = element_text(size=13), panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black")
                # ,legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97)
								 )                     #LEGNED AT RHS
p1 <- p1 + titlesettings + ggtitle('DNI Irradiance - IN-025')
p1 <- p1 + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))

ggsave("/home/admin/Graphs/DNI.pdf", p1, width = 11, height=8)

p1 <- ggplot() + theme_bw()
p1 <- p1 + geom_line(data=datawrite, aes(x=as.Date(Date), y =Ratio), size = 1.5)
p1 <- p1 +  ylab("Ratio") + xlab("") + coord_cartesian(ylim = c(0,2))
p1 <- p1 + scale_x_date(date_breaks = "3 months",date_labels = "%b/%y") + scale_y_continuous(breaks=seq(0, 2, 0.2))
p1 <- p1 + theme(axis.title.x = element_text(size=13), axis.title.y = element_text(size=13),
                 axis.text = element_text(size=13), panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black")
                # ,legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97)
								 )                     #LEGNED AT RHS
p1 <- p1 + titlesettings + ggtitle('GTI/DNI Irradiance Ratio - IN-025')
p1 <- p1 + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))

ggsave("/home/admin/Graphs/Ratio.pdf", p1, width = 11, height=8)

p1 <- ggplot() + theme_bw()
p1 <- p1 + geom_point(data=datawrite, aes(x=GTI, y =DNI), size = 1.5)
p1 <- p1 +  ylab("DNI [kWh/m2]") + xlab("GTI kWh/m2") + coord_cartesian(ylim = c(0,10))
p1 <- p1 + scale_y_continuous(breaks=seq(0, 10, 1)) + scale_x_continuous(breaks = seq(0,10,1))
p1 <- p1 + theme(axis.title.x = element_text(size=13), axis.title.y = element_text(size=13),
                 axis.text = element_text(size=13), panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black")
                # ,legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97)
								 )                     #LEGNED AT RHS
p1 <- p1 + titlesettings + ggtitle('GTI vs DNI - IN-025')
p1 <- p1 + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))
p1 <- p1 + geom_abline(intercept=0,slope=1,color="red")
ggsave("/home/admin/Graphs/GTIvsDNIScatter.pdf", p1, width = 11, height=8)

p1 <- ggplot() + theme_bw()
p1 <- p1 + geom_point(data=datawrite, aes(x=GTI, y =GHIGIS), size = 1.5)
p1 <- p1 +  ylab("GHI [kWh/m2]") + xlab("GTI kWh/m2") + coord_cartesian(ylim = c(0,10))
p1 <- p1 + scale_y_continuous(breaks=seq(0, 10, 1)) + scale_x_continuous(breaks = seq(0,10,1))
p1 <- p1 + theme(axis.title.x = element_text(size=13), axis.title.y = element_text(size=13),
                 axis.text = element_text(size=13), panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black")
                # ,legend.justification = c(0, 0.97), legend.position = c(0.1, 0.97)
								 )                     #LEGNED AT RHS
p1 <- p1 + titlesettings + ggtitle('GTI - IN-025 vs GHI from Chaksu')
p1 <- p1 + theme(legend.text=element_text(size=9), legend.title=element_text(size=11))
p1 <- p1 + geom_abline(intercept=0,slope=1,color="red")

ggsave("/home/admin/Graphs/GTI IN025 vs GHI GIS Chaksu.pdf", p1, width = 11, height=8)

