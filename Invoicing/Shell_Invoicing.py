import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
from email.mime.text import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders

path='/home/admin/Dropbox/Gen 1 Data/'
stns=['[MY-401L]','[MY-402L]','[MY-403L]','[MY-404L]','[MY-405L]','[MY-406L]']
year_month='2020-06'

def send_mail(date,info,rec,attachment_path_list=None):
    s = smtplib.SMTP("smtp.office365.com")
    s.starttls()
    s.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    msg = MIMEMultipart()
    sender = 'operations@cleantechsolar.com'
    recipients = ['sai.pranav@cleantechsolar.com']
    msg['Subject'] = "Shell Invoicing Report"
    if sender is not None:
        msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
            try:
                file_name=each_file_path.split("/")[-1]
                part = MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())
                Encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
            except:
                print ("could not attache file")
    msg.attach(MIMEText(info,'plain/text'))
    s.sendmail(sender, recipients, msg.as_string())

def convert_date(date):
    day=date[8:10]
    month=date[5:7]
    year=date[0:4]
    return month+'/'+day+'/'+year

shell_df=pd.DataFrame()
for index,i in enumerate(stns):
    append_df=pd.DataFrame(columns=['Date','LastR','Eac','DA'])
    for j in sorted(os.listdir(path+i)):
        for k in sorted(os.listdir(path+i+'/'+j)):
            if(k==year_month):
                for l in sorted(os.listdir(path+i+'/'+j+'/'+k)):
                    if 'MFM' in l:
                        for m in sorted(os.listdir(path+i+'/'+j+'/'+k+'/'+l)):
                            df=pd.read_csv(path+i+'/'+j+'/'+k+'/'+l+'/'+m,sep='\t')
                            df_eac=df['TotWhExp_max'].dropna()
                            if(df_eac.empty):
                                da=0
                                if(da>100):
                                    da=100
                                eac='NA'
                                lastr='NA'
                            else:
                                da=float(float(len(df_eac))/288)*100
                                if(da>100):
                                    da=100
                                eac=round((df_eac.iloc[len(df_eac)-1]-df_eac.iloc[0])/1000,1)
                                eac=round(eac)
                                lastr=df_eac.iloc[len(df_eac)-1]/1000
                            final_df=pd.DataFrame({"Date":[m.split('MFM1')[1][1:-4]],'LastR':[lastr],'Eac':[eac],'DA':[da]},columns=['Date','LastR','Eac','DA'])
                            append_df=append_df.append(final_df)
                            append_df.reset_index(drop=True, inplace=True) 
    shell_df['Eac_'+i]=append_df['Eac']

shell_df['Date']=append_df['Date']
cols = shell_df.columns.tolist()
cols = cols[-1:] + cols[:-1]
shell_df = shell_df[cols] 
print(shell_df['Date'])
shell_df.loc[-1] = ['Date', 'Daily Generation (kWh)', '', '', '', '', ''] 
shell_df.loc[-2] = ['Capacity (kWp)', '70.88', '70.88','58.320','70.88','64.8','70.88']   # adding a row
shell_df.loc[-3] = ['Name', 'Shell Elite Putra Heights', 'Shell Putrajaya Precinct 18','Shell Cyberjaya','Shell Mint Hotel','Shell Alor Pongsu Layby NB','Shell Hentian Tangkak']  
shell_df.loc[-4] = ['Cleantech reference', 'MY-401', 'MY-402','MY-403','MY-404','MY-405','MY-406'] 
shell_df.loc[-5] = ['Shell reference', '12163613', '12444271', '12231175', '10311889', '12250922', '12250921'] 

shell_df.index = shell_df.index + 5  # shifting index
shell_df = shell_df.sort_index()  # sorting by index
print(shell_df['Date'])
send_mail('asd',"Please find the report attached.",'gg',['/home/pranav/Shell.csv'])
shell_df.to_csv('/home/pranav/Shell.csv',index=False,header=False)