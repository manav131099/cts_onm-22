require('RCurl')
require('utils')
require('R.utils')
source('/home/admin/CODE/MasterMail/timestamp.R')
FIREMAIL = 0
retryCnt = 0
serverdownmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "PH-001X FTP server down for more than 30'",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
serverupmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "PH-001X FTP server up and running",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
dumpftp = function(days,path,mtno)
{
	print('Calling function')
  url = "ftp://PH-001:HKJ374SD6@ftpnew.cleantechsolar.com/"
  filenames = try(evalWithTimeout(getURLContent(url,ftp.use.epsv = FALSE,dirlistonly = TRUE),
	 timeout = 600, onTimeout = "error"),silent=T)
	if(class(filenames) == 'try-error')
	{
		print('Error in FTP server, will reconnect in 10 mins')
		retryCnt <<- retryCnt + 1
		if(FIREMAIL==0 && retryCnt > 3)
		{
			serverdownmail()
			FIREMAIL<<-1
		}
		return(0)
	}
	retryCnt <<-0
	if(FIREMAIL==1)
	{
		print('Server Back running...')
		serverupmail()
		FIREMAIL<<-0
	}
	print('Passed test')
	if(class(filenames)!="character")
	{
		print('Filenames isnt of type character .. returning')
		return(0)
	}
	recordTimeMaster("PH-001X","FTPProbe")
	newfilenames = unlist(strsplit(filenames,"\n"))
	print('Strsplit done')
  newfilenames = newfilenames[grepl("zip",newfilenames)]
	newfilenames = newfilenames[(!grepl("tmp",newfilenames))]
  newfilenames2 = newfilenames[grepl(c("PH-001"),newfilenames)]
  newfilenames3 = newfilenames[grepl(c("Tmod"),newfilenames)]
  newfilenames3 = newfilenames3[!grepl(c("Tmod_05"),newfilenames3)]
  newfilenames = newfilenames[grepl(c("Gsi00"),newfilenames)]
	newfilenames = newfilenames[!grepl("Gsi00_05",newfilenames)]
  {
	if(length(newfilenames))
		newfilenames=c(newfilenames,newfilenames2,newfilenames3)
	else
		newfilenames = newfilenames2
	}
	print('zip match found')
	days2 = unlist(strsplit(days,"\\."))
	seq1 = seq(from = 1,to = length(days2),by=2)
	days2 = days2[seq1]
	newfilenames2 = unlist(strsplit(newfilenames,"\\."))
	seq1 = seq(from=1,to=length(newfilenames2),by=2)
	newfilenames2 = newfilenames2[seq1]
  match = match(days2,newfilenames2)
	print('Matches found')
	match = match[complete.cases(match)]
	if(length(match) < 1)
	{
	  print('No new files')
		return(1)
	}
  newfilenames = newfilenames[-match]
  if(length(newfilenames) < 1)
	{
	  print('No new files')
		return(1)
	}

	for(y in 1 : length(newfilenames))
  {
    urlnew = paste(url,newfilenames[y],sep="")
    file = try(download.file(urlnew,destfile = paste(path,newfilenames[y],sep="/")),silent = T)
		if(class(file) == 'try-error')
		{
			print(paste('couldnt download',newfilenames[y]))
			next
		}
	  unzipping = try(unzip(paste(path,newfilenames[y],sep="/"),exdir = path),silent = T)
		if(class(unzipping) == 'try-error')
		{
			print(paste('Couldnt extract',newfilenames[y]))
		}
		system(paste('rm "',path,'/',newfilenames[y],'"',sep = ""))
	}
	recordTimeMaster("PH-001X","FTPNewFiles",as.character(newfilenames[length(newfilenames)]))
	return(1)
}
